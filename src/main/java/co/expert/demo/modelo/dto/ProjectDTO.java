package co.expert.demo.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class ProjectDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ProjectDTO.class);
    private Date fechaFinalizacionFormulacion;
    private Date fechaInicioFormulacion;
    private Date fechaPresentacionPropuesta;
    private String financeProj;
    private Integer idPrj;
    private String namePrj;
    private Integer idRslt_ExpectedResult;

    public Date getFechaFinalizacionFormulacion() {
        return fechaFinalizacionFormulacion;
    }

    public void setFechaFinalizacionFormulacion(
        Date fechaFinalizacionFormulacion) {
        this.fechaFinalizacionFormulacion = fechaFinalizacionFormulacion;
    }

    public Date getFechaInicioFormulacion() {
        return fechaInicioFormulacion;
    }

    public void setFechaInicioFormulacion(Date fechaInicioFormulacion) {
        this.fechaInicioFormulacion = fechaInicioFormulacion;
    }

    public Date getFechaPresentacionPropuesta() {
        return fechaPresentacionPropuesta;
    }

    public void setFechaPresentacionPropuesta(Date fechaPresentacionPropuesta) {
        this.fechaPresentacionPropuesta = fechaPresentacionPropuesta;
    }

    public String getFinanceProj() {
        return financeProj;
    }

    public void setFinanceProj(String financeProj) {
        this.financeProj = financeProj;
    }

    public Integer getIdPrj() {
        return idPrj;
    }

    public void setIdPrj(Integer idPrj) {
        this.idPrj = idPrj;
    }

    public String getNamePrj() {
        return namePrj;
    }

    public void setNamePrj(String namePrj) {
        this.namePrj = namePrj;
    }

    public Integer getIdRslt_ExpectedResult() {
        return idRslt_ExpectedResult;
    }

    public void setIdRslt_ExpectedResult(Integer idRslt_ExpectedResult) {
        this.idRslt_ExpectedResult = idRslt_ExpectedResult;
    }
}
