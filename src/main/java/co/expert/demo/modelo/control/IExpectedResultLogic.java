package co.expert.demo.modelo.control;

import co.expert.demo.modelo.ExpectedResult;
import co.expert.demo.modelo.dto.ExpectedResultDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IExpectedResultLogic {
    public List<ExpectedResult> getExpectedResult() throws Exception;

    /**
         * Save an new ExpectedResult entity
         */
    public void saveExpectedResult(ExpectedResult entity)
        throws Exception;

    /**
         * Delete an existing ExpectedResult entity
         *
         */
    public void deleteExpectedResult(ExpectedResult entity)
        throws Exception;

    /**
        * Update an existing ExpectedResult entity
        *
        */
    public void updateExpectedResult(ExpectedResult entity)
        throws Exception;

    /**
         * Load an existing ExpectedResult entity
         *
         */
    public ExpectedResult getExpectedResult(Integer idRslt)
        throws Exception;

    public List<ExpectedResult> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<ExpectedResult> findPageExpectedResult(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberExpectedResult() throws Exception;

    public List<ExpectedResultDTO> getDataExpectedResult()
        throws Exception;
}
