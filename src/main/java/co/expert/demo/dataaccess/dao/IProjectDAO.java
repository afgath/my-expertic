package co.expert.demo.dataaccess.dao;

import co.expert.demo.dataaccess.api.Dao;
import co.expert.demo.modelo.Project;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   ProjectDAO.
*
*/
public interface IProjectDAO extends Dao<Project, Integer> {
}
