package co.expert.demo.modelo.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class ExpectedResultDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ExpectedResultDTO.class);
    private Integer idRslt;
    private String nameRslt;

    public Integer getIdRslt() {
        return idRslt;
    }

    public void setIdRslt(Integer idRslt) {
        this.idRslt = idRslt;
    }

    public String getNameRslt() {
        return nameRslt;
    }

    public void setNameRslt(String nameRslt) {
        this.nameRslt = nameRslt;
    }
}
