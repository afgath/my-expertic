package co.expert.demo.presentation.backingBeans;

import co.expert.demo.exceptions.*;
import co.expert.demo.modelo.*;
import co.expert.demo.modelo.dto.ExpectedResultDTO;
import co.expert.demo.presentation.businessDelegate.*;
import co.expert.demo.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class ExpectedResultView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ExpectedResultView.class);
    private InputText txtNameRslt;
    private InputText txtIdRslt;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<ExpectedResultDTO> data;
    private ExpectedResultDTO selectedExpectedResult;
    private ExpectedResult entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public ExpectedResultView() {
        super();
    }

    public void rowEventListener(RowEditEvent e) {
        try {
            ExpectedResultDTO expectedResultDTO = (ExpectedResultDTO) e.getObject();

            if (txtNameRslt == null) {
                txtNameRslt = new InputText();
            }

            txtNameRslt.setValue(expectedResultDTO.getNameRslt());

            if (txtIdRslt == null) {
                txtIdRslt = new InputText();
            }

            txtIdRslt.setValue(expectedResultDTO.getIdRslt());

            Integer idRslt = FacesUtils.checkInteger(txtIdRslt);
            entity = businessDelegatorView.getExpectedResult(idRslt);

            action_modify();
        } catch (Exception ex) {
        }
    }

    public String action_new() {
        action_clear();
        selectedExpectedResult = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedExpectedResult = null;

        if (txtNameRslt != null) {
            txtNameRslt.setValue(null);
            txtNameRslt.setDisabled(true);
        }

        if (txtIdRslt != null) {
            txtIdRslt.setValue(null);
            txtIdRslt.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtId() {
        try {
            Integer idRslt = FacesUtils.checkInteger(txtIdRslt);
            entity = (idRslt != null)
                ? businessDelegatorView.getExpectedResult(idRslt) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtNameRslt.setDisabled(false);
            txtIdRslt.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtNameRslt.setValue(entity.getNameRslt());
            txtNameRslt.setDisabled(false);
            txtIdRslt.setValue(entity.getIdRslt());
            txtIdRslt.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedExpectedResult = (ExpectedResultDTO) (evt.getComponent()
                                                         .getAttributes()
                                                         .get("selectedExpectedResult"));
        txtNameRslt.setValue(selectedExpectedResult.getNameRslt());
        txtNameRslt.setDisabled(false);
        txtIdRslt.setValue(selectedExpectedResult.getIdRslt());
        txtIdRslt.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedExpectedResult == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new ExpectedResult();

            Integer idRslt = FacesUtils.checkInteger(txtIdRslt);

            entity.setIdRslt(idRslt);
            entity.setNameRslt(FacesUtils.checkString(txtNameRslt));
            businessDelegatorView.saveExpectedResult(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Integer idRslt = new Integer(selectedExpectedResult.getIdRslt());
                entity = businessDelegatorView.getExpectedResult(idRslt);
            }

            entity.setNameRslt(FacesUtils.checkString(txtNameRslt));
            businessDelegatorView.updateExpectedResult(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedExpectedResult = (ExpectedResultDTO) (evt.getComponent()
                                                             .getAttributes()
                                                             .get("selectedExpectedResult"));

            Integer idRslt = new Integer(selectedExpectedResult.getIdRslt());
            entity = businessDelegatorView.getExpectedResult(idRslt);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Integer idRslt = FacesUtils.checkInteger(txtIdRslt);
            entity = businessDelegatorView.getExpectedResult(idRslt);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteExpectedResult(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String actionDeleteDataTableEditable(ActionEvent evt) {
        try {
            selectedExpectedResult = (ExpectedResultDTO) (evt.getComponent()
                                                             .getAttributes()
                                                             .get("selectedExpectedResult"));

            Integer idRslt = new Integer(selectedExpectedResult.getIdRslt());
            entity = businessDelegatorView.getExpectedResult(idRslt);
            businessDelegatorView.deleteExpectedResult(entity);
            data.remove(selectedExpectedResult);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modifyWitDTO(Integer idRslt, String nameRslt)
        throws Exception {
        try {
            entity.setNameRslt(FacesUtils.checkString(nameRslt));
            businessDelegatorView.updateExpectedResult(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("ExpectedResultView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtNameRslt() {
        return txtNameRslt;
    }

    public void setTxtNameRslt(InputText txtNameRslt) {
        this.txtNameRslt = txtNameRslt;
    }

    public InputText getTxtIdRslt() {
        return txtIdRslt;
    }

    public void setTxtIdRslt(InputText txtIdRslt) {
        this.txtIdRslt = txtIdRslt;
    }

    public List<ExpectedResultDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataExpectedResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<ExpectedResultDTO> expectedResultDTO) {
        this.data = expectedResultDTO;
    }

    public ExpectedResultDTO getSelectedExpectedResult() {
        return selectedExpectedResult;
    }

    public void setSelectedExpectedResult(ExpectedResultDTO expectedResult) {
        this.selectedExpectedResult = expectedResult;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
