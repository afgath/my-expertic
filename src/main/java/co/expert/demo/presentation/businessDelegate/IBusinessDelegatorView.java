package co.expert.demo.presentation.businessDelegate;

import co.expert.demo.modelo.ExpectedResult;
import co.expert.demo.modelo.Project;
import co.expert.demo.modelo.control.ExpectedResultLogic;
import co.expert.demo.modelo.control.IExpectedResultLogic;
import co.expert.demo.modelo.control.IProjectLogic;
import co.expert.demo.modelo.control.ProjectLogic;
import co.expert.demo.modelo.dto.ExpectedResultDTO;
import co.expert.demo.modelo.dto.ProjectDTO;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IBusinessDelegatorView {
    public List<ExpectedResult> getExpectedResult() throws Exception;

    public void saveExpectedResult(ExpectedResult entity)
        throws Exception;

    public void deleteExpectedResult(ExpectedResult entity)
        throws Exception;

    public void updateExpectedResult(ExpectedResult entity)
        throws Exception;

    public ExpectedResult getExpectedResult(Integer idRslt)
        throws Exception;

    public List<ExpectedResult> findByCriteriaInExpectedResult(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<ExpectedResult> findPageExpectedResult(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberExpectedResult() throws Exception;

    public List<ExpectedResultDTO> getDataExpectedResult()
        throws Exception;

    public List<Project> getProject() throws Exception;

    public void saveProject(Project entity) throws Exception;

    public void deleteProject(Project entity) throws Exception;

    public void updateProject(Project entity) throws Exception;

    public Project getProject(Integer idPrj) throws Exception;

    public List<Project> findByCriteriaInProject(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Project> findPageProject(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberProject() throws Exception;

    public List<ProjectDTO> getDataProject() throws Exception;
}
