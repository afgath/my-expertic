package co.expert.demo.presentation.backingBeans;

import co.expert.demo.exceptions.*;
import co.expert.demo.modelo.*;
import co.expert.demo.modelo.dto.ExpectedResultDTO;
import co.expert.demo.modelo.dto.ProjectDTO;
import co.expert.demo.presentation.businessDelegate.*;
import co.expert.demo.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class ProjectView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ProjectView.class);
    private InputTextarea txtFinanceProj;
    private InputText txtNamePrj;
    private InputText txtIdRslt_ExpectedResult;
    private InputText txtIdPrj;
    private Calendar txtFechaFinalizacionFormulacion;
    private Calendar txtFechaInicioFormulacion;
    private Calendar txtFechaPresentacionPropuesta;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private SelectOneMenu somExp;
    private List<ProjectDTO> data;
    private ProjectDTO selectedProject;
    private Project entity;
    private List<ExpectedResultDTO> data2;
    private List<SelectItem> expectedItem;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public ProjectView() {
        super();
    }

    public void rowEventListener(RowEditEvent e) {
        try {
            ProjectDTO projectDTO = (ProjectDTO) e.getObject();

            if (txtFinanceProj == null) {
                txtFinanceProj = new InputTextarea();
            }

            txtFinanceProj.setValue(projectDTO.getFinanceProj());

            if (txtNamePrj == null) {
                txtNamePrj = new InputText();
            }

            txtNamePrj.setValue(projectDTO.getNamePrj());

            if (txtIdRslt_ExpectedResult == null) {
                txtIdRslt_ExpectedResult = new InputText();
            }

            txtIdRslt_ExpectedResult.setValue(projectDTO.getIdRslt_ExpectedResult());

            if (txtIdPrj == null) {
                txtIdPrj = new InputText();
            }

            txtIdPrj.setValue(projectDTO.getIdPrj());

            if (txtFechaFinalizacionFormulacion == null) {
                txtFechaFinalizacionFormulacion = new Calendar();
            }

            txtFechaFinalizacionFormulacion.setValue(projectDTO.getFechaFinalizacionFormulacion());

            if (txtFechaInicioFormulacion == null) {
                txtFechaInicioFormulacion = new Calendar();
            }

            txtFechaInicioFormulacion.setValue(projectDTO.getFechaInicioFormulacion());

            if (txtFechaPresentacionPropuesta == null) {
                txtFechaPresentacionPropuesta = new Calendar();
            }

            txtFechaPresentacionPropuesta.setValue(projectDTO.getFechaPresentacionPropuesta());

            Integer idPrj = FacesUtils.checkInteger(txtIdPrj);
            entity = businessDelegatorView.getProject(idPrj);

            action_modify();
        } catch (Exception ex) {
        }
    }

    public String action_new() {
        action_clear();
        selectedProject = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedProject = null;

        if (txtFinanceProj != null) {
            txtFinanceProj.setValue(null);
            txtFinanceProj.setDisabled(false);
        }

        if (txtNamePrj != null) {
            txtNamePrj.setValue(null);
            txtNamePrj.setDisabled(false);
        }

        if (txtIdRslt_ExpectedResult != null) {
            txtIdRslt_ExpectedResult.setValue(null);
            txtIdRslt_ExpectedResult.setDisabled(false);
        }

        if (txtFechaFinalizacionFormulacion != null) {
            txtFechaFinalizacionFormulacion.setValue(null);
            txtFechaFinalizacionFormulacion.setDisabled(false);
        }

        if (txtFechaInicioFormulacion != null) {
            txtFechaInicioFormulacion.setValue(null);
            txtFechaInicioFormulacion.setDisabled(false);
        }

        if (txtFechaPresentacionPropuesta != null) {
            txtFechaPresentacionPropuesta.setValue(null);
            txtFechaPresentacionPropuesta.setDisabled(false);
        }

        if (txtIdPrj != null) {
            txtIdPrj.setValue(null);
            txtIdPrj.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(false);
        }


        return "";
    }

    public void listener_txtFechaFinalizacionFormulacion() {
        Date inputDate = (Date) txtFechaFinalizacionFormulacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaInicioFormulacion() {
        Date inputDate = (Date) txtFechaInicioFormulacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaPresentacionPropuesta() {
        Date inputDate = (Date) txtFechaPresentacionPropuesta.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {

        if (entity == null) {
            txtFinanceProj.setDisabled(false);
            txtNamePrj.setDisabled(false);
            txtIdRslt_ExpectedResult.setDisabled(false);
            txtFechaFinalizacionFormulacion.setDisabled(false);
            txtFechaInicioFormulacion.setDisabled(false);
            txtFechaPresentacionPropuesta.setDisabled(false);
            txtIdPrj.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtFechaFinalizacionFormulacion.setValue(entity.getFechaFinalizacionFormulacion());
            txtFechaFinalizacionFormulacion.setDisabled(false);
            txtFechaInicioFormulacion.setValue(entity.getFechaInicioFormulacion());
            txtFechaInicioFormulacion.setDisabled(false);
            txtFechaPresentacionPropuesta.setValue(entity.getFechaPresentacionPropuesta());
            txtFechaPresentacionPropuesta.setDisabled(false);
            txtFinanceProj.setValue(entity.getFinanceProj());
            txtFinanceProj.setDisabled(false);
            txtNamePrj.setValue(entity.getNamePrj());
            txtNamePrj.setDisabled(false);
            txtIdRslt_ExpectedResult.setValue(entity.getExpectedResult()
                                                    .getIdRslt());
            txtIdRslt_ExpectedResult.setDisabled(false);
            txtIdPrj.setValue(entity.getIdPrj());
            txtIdPrj.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedProject = (ProjectDTO) (evt.getComponent().getAttributes()
                                           .get("selectedProject"));
        txtFechaFinalizacionFormulacion.setValue(selectedProject.getFechaFinalizacionFormulacion());
        txtFechaFinalizacionFormulacion.setDisabled(false);
        txtFechaInicioFormulacion.setValue(selectedProject.getFechaInicioFormulacion());
        txtFechaInicioFormulacion.setDisabled(false);
        txtFechaPresentacionPropuesta.setValue(selectedProject.getFechaPresentacionPropuesta());
        txtFechaPresentacionPropuesta.setDisabled(false);
        txtFinanceProj.setValue(selectedProject.getFinanceProj());
        txtFinanceProj.setDisabled(false);
        txtNamePrj.setValue(selectedProject.getNamePrj());
        txtNamePrj.setDisabled(false);
        txtIdRslt_ExpectedResult.setValue(selectedProject.getIdRslt_ExpectedResult());
        txtIdRslt_ExpectedResult.setDisabled(false);
        txtIdPrj.setValue(selectedProject.getIdPrj());
        txtIdPrj.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            action_create();
            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Project();
            //entity.setIdPrj(null);
            entity.setFechaFinalizacionFormulacion(FacesUtils.checkDate(
                    txtFechaFinalizacionFormulacion));
            entity.setFechaInicioFormulacion(FacesUtils.checkDate(
                    txtFechaInicioFormulacion));
            entity.setFechaPresentacionPropuesta(FacesUtils.checkDate(
                    txtFechaPresentacionPropuesta));
            entity.setFinanceProj(FacesUtils.checkString(txtFinanceProj));
            entity.setNamePrj(FacesUtils.checkString(txtNamePrj));
            ExpectedResult expectedResult = businessDelegatorView.getExpectedResult(Integer.parseInt(somExp.getValue().toString()));
            entity.setExpectedResult(expectedResult);
            businessDelegatorView.saveProject(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Integer idPrj = new Integer(selectedProject.getIdPrj());
                entity = businessDelegatorView.getProject(idPrj);
            }

            entity.setFechaFinalizacionFormulacion(FacesUtils.checkDate(
                    txtFechaFinalizacionFormulacion));
            entity.setFechaInicioFormulacion(FacesUtils.checkDate(
                    txtFechaInicioFormulacion));
            entity.setFechaPresentacionPropuesta(FacesUtils.checkDate(
                    txtFechaPresentacionPropuesta));
            entity.setFinanceProj(FacesUtils.checkString(txtFinanceProj));
            entity.setNamePrj(FacesUtils.checkString(txtNamePrj));
            entity.setExpectedResult((FacesUtils.checkInteger(
                    txtIdRslt_ExpectedResult) != null)
                ? businessDelegatorView.getExpectedResult(
                    FacesUtils.checkInteger(txtIdRslt_ExpectedResult)) : null);
            businessDelegatorView.updateProject(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedProject = (ProjectDTO) (evt.getComponent().getAttributes()
                                               .get("selectedProject"));

            Integer idPrj = new Integer(selectedProject.getIdPrj());
            entity = businessDelegatorView.getProject(idPrj);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Integer idPrj = FacesUtils.checkInteger(txtIdPrj);
            entity = businessDelegatorView.getProject(idPrj);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteProject(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String actionDeleteDataTableEditable(ActionEvent evt) {
        try {
            selectedProject = (ProjectDTO) (evt.getComponent().getAttributes()
                                               .get("selectedProject"));

            Integer idPrj = new Integer(selectedProject.getIdPrj());
            entity = businessDelegatorView.getProject(idPrj);
            businessDelegatorView.deleteProject(entity);
            data.remove(selectedProject);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modifyWitDTO(Date fechaFinalizacionFormulacion,
        Date fechaInicioFormulacion, Date fechaPresentacionPropuesta,
        String financeProj, Integer idPrj, String namePrj,
        Integer idRslt_ExpectedResult) throws Exception {
        try {
            entity.setFechaFinalizacionFormulacion(FacesUtils.checkDate(
                    fechaFinalizacionFormulacion));
            entity.setFechaInicioFormulacion(FacesUtils.checkDate(
                    fechaInicioFormulacion));
            entity.setFechaPresentacionPropuesta(FacesUtils.checkDate(
                    fechaPresentacionPropuesta));
            entity.setFinanceProj(FacesUtils.checkString(financeProj));
            entity.setNamePrj(FacesUtils.checkString(namePrj));
            businessDelegatorView.updateProject(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("ProjectView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public List<ExpectedResultDTO> getDataExpRes() {
        try {
            if (data2 == null) {
                data2 = businessDelegatorView.getDataExpectedResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data2;
    }

    public List<SelectItem> getExpectedResults() throws Exception {
		if (expectedItem == null) {
			expectedItem = new ArrayList<SelectItem>();
			List<ExpectedResultDTO> expectedResults = businessDelegatorView.getDataExpectedResult();
			log.info("Sí carga el wey SelectItem");
			for (ExpectedResultDTO expectedResult : expectedResults) {
				expectedItem
						.add(new SelectItem(expectedResult.getIdRslt(), String.valueOf(expectedResult.getNameRslt())));
			}
		}
		return expectedItem;
	}

	public void setExpectedResults(List<SelectItem> expectedItem) {
		this.expectedItem = expectedItem;
	}
    
    public void getDataExpRes(List<ExpectedResultDTO> expectedResultDTO) {
        this.data2 = expectedResultDTO;
    }
    
    public InputTextarea getTxtFinanceProj() {
        return txtFinanceProj;
    }

    public void setTxtFinanceProj(InputTextarea txtFinanceProj) {
        this.txtFinanceProj = txtFinanceProj;
    }

    public InputText getTxtNamePrj() {
        return txtNamePrj;
    }

    public void setTxtNamePrj(InputText txtNamePrj) {
        this.txtNamePrj = txtNamePrj;
    }

    public InputText getTxtIdRslt_ExpectedResult() {
        return txtIdRslt_ExpectedResult;
    }

    public void setTxtIdRslt_ExpectedResult(InputText txtIdRslt_ExpectedResult) {
        this.txtIdRslt_ExpectedResult = txtIdRslt_ExpectedResult;
    }

    public Calendar getTxtFechaFinalizacionFormulacion() {
        return txtFechaFinalizacionFormulacion;
    }

    public void setTxtFechaFinalizacionFormulacion(
        Calendar txtFechaFinalizacionFormulacion) {
        this.txtFechaFinalizacionFormulacion = txtFechaFinalizacionFormulacion;
    }

    public Calendar getTxtFechaInicioFormulacion() {
        return txtFechaInicioFormulacion;
    }

    public void setTxtFechaInicioFormulacion(Calendar txtFechaInicioFormulacion) {
        this.txtFechaInicioFormulacion = txtFechaInicioFormulacion;
    }

    public Calendar getTxtFechaPresentacionPropuesta() {
        return txtFechaPresentacionPropuesta;
    }

    public void setTxtFechaPresentacionPropuesta(
        Calendar txtFechaPresentacionPropuesta) {
        this.txtFechaPresentacionPropuesta = txtFechaPresentacionPropuesta;
    }

    public InputText getTxtIdPrj() {
        return txtIdPrj;
    }

    public void setTxtIdPrj(InputText txtIdPrj) {
        this.txtIdPrj = txtIdPrj;
    }

    public List<ProjectDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataProject();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<ProjectDTO> projectDTO) {
        this.data = projectDTO;
    }

    public ProjectDTO getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(ProjectDTO project) {
        this.selectedProject = project;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    
    public SelectOneMenu getSomExp() {
		return somExp;
	}

	public void setSomExp(SelectOneMenu somExp) {
		this.somExp = somExp;
	}

	public Project getEntity() {
		return entity;
	}

	public void setEntity(Project entity) {
		this.entity = entity;
	}

	public List<ExpectedResultDTO> getData2() {
		return data2;
	}

	public void setData2(List<ExpectedResultDTO> data2) {
		this.data2 = data2;
	}

	public List<SelectItem> getExpectedItem() {
		return expectedItem;
	}

	public void setExpectedItem(List<SelectItem> expectedItem) {
		this.expectedItem = expectedItem;
	}

	public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}
