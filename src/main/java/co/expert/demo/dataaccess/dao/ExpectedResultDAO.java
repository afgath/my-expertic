package co.expert.demo.dataaccess.dao;

import co.expert.demo.dataaccess.api.JpaDaoImpl;
import co.expert.demo.modelo.ExpectedResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * ExpectedResult entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.ExpectedResult
 */
@Scope("singleton")
@Repository("ExpectedResultDAO")
public class ExpectedResultDAO extends JpaDaoImpl<ExpectedResult, Integer>
    implements IExpectedResultDAO {
    private static final Logger log = LoggerFactory.getLogger(ExpectedResultDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IExpectedResultDAO getFromApplicationContext(
        ApplicationContext ctx) {
        return (IExpectedResultDAO) ctx.getBean("ExpectedResultDAO");
    }
}
