package co.expert.demo.dataaccess.dao;

import co.expert.demo.dataaccess.api.Dao;
import co.expert.demo.modelo.ExpectedResult;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   ExpectedResultDAO.
*
*/
public interface IExpectedResultDAO extends Dao<ExpectedResult, Integer> {
}
