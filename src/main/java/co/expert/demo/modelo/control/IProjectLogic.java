package co.expert.demo.modelo.control;

import co.expert.demo.modelo.Project;
import co.expert.demo.modelo.dto.ProjectDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IProjectLogic {
    public List<Project> getProject() throws Exception;

    /**
         * Save an new Project entity
         */
    public void saveProject(Project entity) throws Exception;

    /**
         * Delete an existing Project entity
         *
         */
    public void deleteProject(Project entity) throws Exception;

    /**
        * Update an existing Project entity
        *
        */
    public void updateProject(Project entity) throws Exception;

    /**
         * Load an existing Project entity
         *
         */
    public Project getProject(Integer idPrj) throws Exception;

    public List<Project> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Project> findPageProject(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberProject() throws Exception;

    public List<ProjectDTO> getDataProject() throws Exception;
}
