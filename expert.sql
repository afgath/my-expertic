PGDMP     "            	    
    v            expertic_prj_inv    9.6.10    9.6.10     \           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            ]           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            ^           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            _           1262    16393    expertic_prj_inv    DATABASE     �   CREATE DATABASE expertic_prj_inv WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
     DROP DATABASE expertic_prj_inv;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            `           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            a           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16419    expected_result    TABLE     t   CREATE TABLE public.expected_result (
    id_rslt integer NOT NULL,
    name_rslt character varying(50) NOT NULL
);
 #   DROP TABLE public.expected_result;
       public         postgres    false    3            �            1259    16417    expected_result_id_rslt_seq    SEQUENCE     �   CREATE SEQUENCE public.expected_result_id_rslt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.expected_result_id_rslt_seq;
       public       postgres    false    3    186            b           0    0    expected_result_id_rslt_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.expected_result_id_rslt_seq OWNED BY public.expected_result.id_rslt;
            public       postgres    false    185            �            1259    16427    project    TABLE     =  CREATE TABLE public.project (
    id_prj integer NOT NULL,
    name_prj character varying(100) NOT NULL,
    exp_rslts integer,
    finance_proj character varying NOT NULL,
    fecha_inicio_formulacion date NOT NULL,
    fecha_finalizacion_formulacion date NOT NULL,
    fecha_presentacion_propuesta date NOT NULL
);
    DROP TABLE public.project;
       public         postgres    false    3            �            1259    16425    project_id_prj_seq    SEQUENCE     {   CREATE SEQUENCE public.project_id_prj_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.project_id_prj_seq;
       public       postgres    false    188    3            c           0    0    project_id_prj_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.project_id_prj_seq OWNED BY public.project.id_prj;
            public       postgres    false    187            �           2604    16422    expected_result id_rslt    DEFAULT     �   ALTER TABLE ONLY public.expected_result ALTER COLUMN id_rslt SET DEFAULT nextval('public.expected_result_id_rslt_seq'::regclass);
 F   ALTER TABLE public.expected_result ALTER COLUMN id_rslt DROP DEFAULT;
       public       postgres    false    185    186    186            �           2604    16430    project id_prj    DEFAULT     p   ALTER TABLE ONLY public.project ALTER COLUMN id_prj SET DEFAULT nextval('public.project_id_prj_seq'::regclass);
 =   ALTER TABLE public.project ALTER COLUMN id_prj DROP DEFAULT;
       public       postgres    false    187    188    188            W          0    16419    expected_result 
   TABLE DATA               =   COPY public.expected_result (id_rslt, name_rslt) FROM stdin;
    public       postgres    false    186   V       d           0    0    expected_result_id_rslt_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.expected_result_id_rslt_seq', 3, true);
            public       postgres    false    185            Y          0    16427    project 
   TABLE DATA               �   COPY public.project (id_prj, name_prj, exp_rslts, finance_proj, fecha_inicio_formulacion, fecha_finalizacion_formulacion, fecha_presentacion_propuesta) FROM stdin;
    public       postgres    false    188   �       e           0    0    project_id_prj_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.project_id_prj_seq', 6, true);
            public       postgres    false    187            �           2606    16424 $   expected_result expected_result_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.expected_result
    ADD CONSTRAINT expected_result_pkey PRIMARY KEY (id_rslt);
 N   ALTER TABLE ONLY public.expected_result DROP CONSTRAINT expected_result_pkey;
       public         postgres    false    186    186            �           2606    16437    project project_name_prj_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_name_prj_key UNIQUE (name_prj);
 F   ALTER TABLE ONLY public.project DROP CONSTRAINT project_name_prj_key;
       public         postgres    false    188    188            �           2606    16435    project project_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id_prj);
 >   ALTER TABLE ONLY public.project DROP CONSTRAINT project_pkey;
       public         postgres    false    188    188            �           2606    16438    project project_exp_rslts_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_exp_rslts_fkey FOREIGN KEY (exp_rslts) REFERENCES public.expected_result(id_rslt);
 H   ALTER TABLE ONLY public.project DROP CONSTRAINT project_exp_rslts_fkey;
       public       postgres    false    186    2011    188            W   <   x�3�(J-N�+IL�<�9O�3�$�(/�ˈ�9?�,?9�$�(3.l�*�Z�����        Y      x������ � �     